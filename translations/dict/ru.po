# Translators:
# Andrei Karas <akaras@inbox.ru>, 2017
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2017-04-05 00:52+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Andrei Karas <akaras@inbox.ru>, 2017\n"
"Language-Team: Russian (https://www.transifex.com/akaras/teams/959/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#. (itstool) path: texts/text
#: texts.xml:6
msgid "hello"
msgstr "привет"

#. (itstool) path: texts/text
#: texts.xml:7
msgid "bye"
msgstr "пока"

#. (itstool) path: texts/text
#: texts.xml:8
msgid "follow me"
msgstr "следуй за мной"

#. (itstool) path: texts/text
#: texts.xml:9
msgid "follow"
msgstr "следуй"

#. (itstool) path: texts/text
#: texts.xml:10
msgid "stop"
msgstr "стоп"

#. (itstool) path: texts/text
#: texts.xml:11
msgid "help"
msgstr "помогите"

#. (itstool) path: texts/text
#: texts.xml:12
msgid "go away"
msgstr "иди отсюда"

#. (itstool) path: texts/text
#: texts.xml:13
msgid "bot"
msgstr "бот"
